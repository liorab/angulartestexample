// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDqACdN4uVovhrU0lkmHauSaL8KAaJSOnw",
    authDomain: "angularte-ab810.firebaseapp.com",
    databaseURL: "https://angularte-ab810.firebaseio.com",
    projectId: "angularte-ab810",
    storageBucket: "angularte-ab810.appspot.com",
    messagingSenderId: "31018847861",
    appId: "1:31018847861:web:ea59b579bc89ce5109323c",
    measurementId: "G-G3H7RMW5WD"
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
