import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>

  constructor(public afAuth:AngularFireAuth,
    private router:Router) {
      this.user=this.afAuth.authState;
}
signup(email:string,password:string){
  this.afAuth.auth.createUserWithEmailAndPassword(email,password)
  .then(()=>{
    this.router.navigate(['/wellcome']);
  }).catch((error) => {
    window.alert(error.message)
  })    
}
login(email:string,password:string){
  return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(() => {
            this.router.navigate(['/wellcome']);
      }).catch((error) => {
        window.alert(error.message)
      })
  }
  Logout(){
    this.afAuth.auth.signOut().then(res=> console.log('successful logout'));
  }
}
