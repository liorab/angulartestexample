import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowClassifyComponent } from './show-classify.component';

describe('ShowClassifyComponent', () => {
  let component: ShowClassifyComponent;
  let fixture: ComponentFixture<ShowClassifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowClassifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowClassifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
