import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../classify.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-show-classify',
  templateUrl: './show-classify.component.html',
  styleUrls: ['./show-classify.component.css']
})
export class ShowClassifyComponent implements OnInit {

  classify$:Observable<any>;

  constructor(public router:Router, public classifyService:ClassifyService ) { }

  ngOnInit() {
    this.classify$=this.classifyService.getClassify();
  }

}
